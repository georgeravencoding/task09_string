package com.kolchak;

import com.kolchak.view.MyView;
import org.apache.logging.log4j.*;

public class Application {
    Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {

        new MyView().show();
    }
}

package com.kolchak.controller;

import com.kolchak.model.ReadFile;
import com.kolchak.utils.StringUtils;

import java.util.List;
import java.util.Scanner;

public class ControllerImpl implements Controller{
    private ReadFile fileService;

    public ControllerImpl(ReadFile fileService) {
        this.fileService = fileService;
    }

    @Override
    public void concatParams() {
    }

    @Override
    public void checkSentence() {
    }

    @Override
    public void splitString() {
    }

    @Override
    public void replaceVowels() {
    }

    @Override
    public void sortTextByWordCount() {
    }

    @Override
    public void findUniqueWordInFirstSentence() {
        List<String> text = fileService.listOfLines();
        System.out.println(StringUtils.findUniqueWordFromFirstSentence(text));
    }

    @Override
    public void questionsUniqueWords() {
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        StringUtils.questionsUnique(fileService.listOfLines(), length);
    }
}


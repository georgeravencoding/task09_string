package com.kolchak.controller;

public interface Controller {
    void concatParams();

    void checkSentence();

    void splitString();

    void replaceVowels();

    void sortTextByWordCount();

    void findUniqueWordInFirstSentence();

    void questionsUniqueWords();
}

package com.kolchak.model;

import java.io.File;
import java.util.List;

public interface ReadFile {
    List<String> listOfLines();
}

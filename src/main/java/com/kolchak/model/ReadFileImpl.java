package com.kolchak.model;

import edu.emory.mathcs.backport.java.util.Arrays;

import java.io.*;
import java.util.List;

public class ReadFileImpl implements ReadFile {
    private ReadFile readFile;
    File file = new File("Thinking in Java.txt");
    String absolutePath = file.getAbsolutePath();
    private String fullText;

    public ReadFileImpl() {
    }

    public String getText() {
        String fullText = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
            }
            fullText = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fullText;
    }

    @Override
    public List<String> listOfLines() {
        return (List<String>) Arrays.asList(getText().split("[.?!]"));
    }
}
